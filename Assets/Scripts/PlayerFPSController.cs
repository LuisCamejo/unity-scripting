﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(CharacterMovement))]
[RequireComponent(typeof(MouseLook))]
public class PlayerFPSController : MonoBehaviour
{ 
    private CharacterMovement characterMovement;
    private MouseLook mouseLook;
    private GunAiming gunAiming;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("PlayerCapsule").GetComponent<MeshRenderer>().enabled = false;

        characterMovement = GetComponent<MouseLook>();
        mouseLook = GetComponent<MouseLook>();
        gunAiming = GetComponentInChildren<GunAiming>();
    }


    private void Update()
    {
        movement();
        rotation();
        aiming();
    }

    private void movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

    characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }

    private void rotation()
    {
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");
    }

    private void aiming()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            gunAiming.OnButtonDown();
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            gunAiming.OnButtonUp();
        }
    }
}
